﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecodingAnagram
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.Now);
            var watch = DateTime.Now;
            const string phraseAnagramm = "poultry outwits ants";

            using (var client = new WebClient())
            {
                client.DownloadFile("http://followthewhiterabbit.trustpilot.com/cs/wordlist", @".\wordList");
            }

            IList<string> dictionary = System.IO.File.ReadAllLines(@".\wordList");
            dictionary = dictionary.Distinct().ToList();
            
            //phraseAnagramm = "poultry afao";
            //var dictionary = new List<string> { "poultry", "ulpotry", "afao", "afa", "a", "trypoul", "jkopoultry" };
            
            var wordSearcher = new WordSearch(new Encryptor("4624d200580677270a54ccff86b9610e"));
            var wordArray = phraseAnagramm.Split(' ');

            using (var cts = new CancellationTokenSource())
            {
               wordSearcher.SearchFraseParallel(string.Empty, dictionary, phraseAnagramm.Replace(" ", string.Empty), wordArray.Length, cts);
            }

            Console.WriteLine("spent {0}", DateTime.Now - watch);
            
            Console.ReadKey();
        }
    }
}
