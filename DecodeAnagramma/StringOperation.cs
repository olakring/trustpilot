﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecodingAnagram
{
    public static class StringOperation
    {
        public static string ExtractChars(string source, string value)
        {
            foreach (var c in value)
            {
                int index = source.IndexOf(c);
                if (index >= 0)
                {
                    source = source.Remove(index, 1);
                }
                else
                {
                    return null;
                }
            }
            return source;
        }
    }
}
