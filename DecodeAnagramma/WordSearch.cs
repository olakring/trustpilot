﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecodingAnagram
{
    internal class WordSearch
    {
        private Encryptor _encryptor;
        public WordSearch(Encryptor encryptor)
        {
            _encryptor = encryptor;
        }

        public string SearchFrase(string phrase, IList<string> dictionary, string template, int wordCount)
        {
            string result = null;
            var maxWordSize = template.Length - wordCount + 1;
            foreach (var word in dictionary.Where(d => d.Length <= maxWordSize))
            {
                var newTemplate = StringOperation.ExtractChars(template, word);
                if (newTemplate == null)
                {
                    continue;
                }

                if (newTemplate != string.Empty && wordCount == 1)
                {
                    continue;
                }
                if (newTemplate == string.Empty && wordCount == 1)
                {
                    var candidate = phrase + word;
                    if (_encryptor.CalculateMd5Hash(candidate)
                                .Equals(_encryptor.Md5Value, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Console.WriteLine("Solution: {0}", candidate);
                        return candidate;
                    }
                }

                if (!string.IsNullOrEmpty(newTemplate) && wordCount > 1)
                {
                    result = SearchFrase(string.Format("{0}{1} ", phrase, word), dictionary, newTemplate, wordCount - 1);
                    if (!string.IsNullOrEmpty(result))
                    {
                        return result;
                    }
                }
            }
            return result;
        }

        public void SearchFraseParallel(string phrase, IList<string> dictionary, string template, int wordCount, CancellationTokenSource cts)
        {
            var maxWordSize = template.Length - wordCount + 1;
            dictionary = dictionary.Where(d => d.Length <= maxWordSize && template.Contains(d[0])).ToList();
            var po = new ParallelOptions();
            po.CancellationToken = cts.Token;
            po.MaxDegreeOfParallelism = 12;
            try
            {
                Parallel.ForEach(
                    dictionary,
                    po,
                    word =>
                    {
                        var newTemplate = StringOperation.ExtractChars(template, word);
                        if (newTemplate == null)
                        {
                            return;
                        }

                        if (newTemplate != string.Empty && wordCount == 1)
                        {
                            return;
                        }
                        if (newTemplate == string.Empty && wordCount == 1)
                        {
                            var candidate = phrase + word;
                            if (_encryptor.CalculateMd5Hash(candidate)
                                        .Equals(_encryptor.Md5Value, StringComparison.InvariantCultureIgnoreCase))
                            {
                                Console.WriteLine("Solution!!!: {0}", candidate);
                                cts.Cancel();
                            }
                        }

                        if (!string.IsNullOrEmpty(newTemplate) && wordCount > 1)
                        {
                            SearchFraseParallel(
                                string.Format("{0}{1} ", phrase, word), dictionary, newTemplate, wordCount - 1, cts);
                        }
                    });
            }
            catch (OperationCanceledException)
            {
                // do nothing, because it is expected error
            }
        }
    }
}
