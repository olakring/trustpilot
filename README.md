# README #

### Application description ###

## Quick summary
Application is designed to search for a phrase by given anagram and expected hash. There were some investigation done and now it search in parallel for needed phrase. 

## Versions
* 1.0
Solution was found

* 2.0
Optimize some productivity to make it faster. So was decided to skip on some dictionary values and to run it in several Threads.